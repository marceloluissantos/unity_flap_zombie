using System.Collections;
using UnityEngine;

public class Rock : MonoBehaviour
{
    [SerializeField]
    private float positionYUp;
    [SerializeField]
    private float positionYDown;
    [SerializeField]
    private float speed = 1;
    [SerializeField]
    private float timeout = 0.2f;

    private Vector3 initialPosition;

    void Start()
    {
        this.initialPosition = base.transform.position;
        base.StartCoroutine(this.MoveRock());
    }

    private IEnumerator MoveRock()
    {
        yield return this.MoveToPosition(Vector3.down);
        yield return this.MoveToPosition(Vector3.up);

        base.StartCoroutine(this.MoveRock());
    }

    private IEnumerator MoveToPosition(Vector3 vector3Direction)
    {
        float positionToMove;

        if (vector3Direction == Vector3.down)
        {
            positionToMove = this.positionYDown;
        }
        else
        {
            positionToMove = this.positionYUp;
        }

        while (Mathf.Abs(base.transform.localPosition.y - positionToMove) > 0.1f)
        {
            base.transform.Translate(vector3Direction * this.speed * Time.deltaTime);
            yield return null;
        }
        
        yield return new WaitForSeconds(timeout);
    }

    public void ResetPosition()
	{
		this.transform.position = this.initialPosition;
	}
}