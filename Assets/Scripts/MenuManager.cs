﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

	[SerializeField]
	private GameObject mainMenu;
	[SerializeField]
	private GameObject gameOverMenu;
	[SerializeField]
	private GameObject scoreMenu;
	[SerializeField]
	private Text textGameOverScore;

	private string platformTag = "Platform";
	private string zombieName = "zombie";

	void Awake()
	{
		Assert.IsNotNull(this.mainMenu);
		Assert.IsNotNull(this.gameOverMenu);
		Assert.IsNotNull(this.scoreMenu);
		Assert.IsNotNull(this.textGameOverScore);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GameOver()
	{
		this.scoreMenu.SetActive(false);
		this.gameOverMenu.SetActive(true);

		this.textGameOverScore.text = GameManager.Instance.Score.ToString();
	}

	public void OnMainMenuClicked()
	{
		this.RestartGame();
		GameManager.Instance.State = EnumState.Idle;
		this.mainMenu.SetActive(true);
	}

	public void OnMenuRestartClicked()
	{
		this.RestartGame();
		this.scoreMenu.SetActive(true);
		GameManager.Instance.Score = 0;
		GameManager.Instance.State = EnumState.GameStarted;
	}

	private void RestartGame()
	{
		this.ResetPlatforms();
		this.ResetZombie();

		this.gameOverMenu.SetActive(false);
	}

	private void ResetPlatforms()
	{
		foreach (GameObject platform in GameObject.FindGameObjectsWithTag(this.platformTag))
		{
			platform.GetComponent<MovableGameObject>().Restart();
		}
	}

	private void ResetZombie()
	{
		GameObject.Find(this.zombieName).GetComponent<Zombie>().Reset();
	}
}
