﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Zombie : MonoBehaviour {

	private bool isJumping;
	private string jumpAnimatioName = "Jump";

	private Animator animatorZombie;
	private Rigidbody rigidBody;
	private SphereCollider sphereCollider;
	private AudioSource audioSource;

	[SerializeField]
	private MenuManager menuManager;

	[SerializeField]
	private AudioClip audioClipJump;
	[SerializeField]
	private AudioClip audioClipDie;
	[SerializeField]
	private AudioClip audioClipScore;

	private Vector3 initialPosition;
	private Quaternion initialRotation;

	void Awake()
	{
		Assert.IsNotNull(this.audioClipJump);
		Assert.IsNotNull(this.audioClipDie);
		Assert.IsNotNull(this.audioClipScore);
		Assert.IsNotNull(this.menuManager);
	}

	// Use this for initialization
	void Start () 
	{
		this.animatorZombie = base.GetComponent<Animator>();	
		this.rigidBody = base.GetComponent<Rigidbody>();
		this.audioSource = base.GetComponent<AudioSource>();
		this.sphereCollider = base.GetComponent<SphereCollider>();

		this.initialPosition = base.transform.position;
		this.initialRotation = base.transform.rotation;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GameManager.Instance.State != EnumState.Dead && GameManager.Instance.State != EnumState.Idle && Input.GetMouseButtonDown(0) == true)
		{
			GameManager.Instance.State = EnumState.Playing;
			this.animatorZombie.Play(jumpAnimatioName);
			this.rigidBody.useGravity = true;
			this.audioSource.PlayOneShot(this.audioClipJump);
			this.isJumping = true;
		}
	}

	void FixedUpdate()
	{
		if (this.isJumping == true)
		{
			this.isJumping = false;
			this.rigidBody.velocity = new Vector2(0, 0);
			this.rigidBody.AddForce(new Vector2(0, 100), ForceMode.Impulse);
		}
	}

	void OnCollisionEnter(Collision other)
	{
		bool isRock = other.gameObject.GetComponent<Rock>() != null;
		bool isFloor = other.gameObject.GetComponent<Floor>() != null;

		if (isRock == true || isFloor)
		{
			GameManager.Instance.State = EnumState.Dead;
			this.sphereCollider.isTrigger = true;
			this.audioSource.PlayOneShot(this.audioClipDie);
			this.rigidBody.AddForce(new Vector2(50, 200), ForceMode.Impulse);

			base.StartCoroutine(this.GameOver());
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (GameManager.Instance.State == EnumState.Playing)
		{
			Coin coin = other.gameObject.GetComponent<Coin>();

			if (coin != null)
			{
				GameManager.Instance.Score = GameManager.Instance.Score + coin.points;
				this.audioSource.PlayOneShot(this.audioClipScore, 15f);
				coin.Disable();
			}
		}
	}

	private IEnumerator GameOver()
	{
		yield return new WaitForSeconds(1f);

		this.menuManager.GameOver();
	}

	public void Reset()
	{
		this.sphereCollider.isTrigger = false;
		this.rigidBody.useGravity = false;
		this.rigidBody.velocity = Vector3.zero;
		this.rigidBody.angularVelocity = Vector3.zero;
		base.transform.position = this.initialPosition;
		base.transform.rotation = this.initialRotation;
	}
}
