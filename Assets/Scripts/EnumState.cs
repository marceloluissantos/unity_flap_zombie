public enum EnumState
{
    Idle,
    GameStarted,
    Playing,
    Dead
}