﻿using UnityEngine;
using UnityEngine.Assertions;

public class MainMenu : MonoBehaviour {

	[SerializeField]
	private GameObject gameObjectScore;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		Assert.IsNotNull(this.gameObjectScore);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPlayClicked()
	{
		GameManager.Instance.Score = 0;
		GameManager.Instance.State = EnumState.GameStarted;
		base.gameObject.SetActive(false);
		this.gameObjectScore.SetActive(true);
	}
}
