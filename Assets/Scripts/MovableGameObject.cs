﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableGameObject : MonoBehaviour {

	[SerializeField]
	private float speed = 10;
	[SerializeField]
	private Vector3 resetPosition;
	[SerializeField]
	private Vector3 initialPosition;
	[SerializeField]
	private List<Coin> listCoin;

	private Vector3 startPosition;

	// Use this for initialization
	void Start () 
	{
		this.startPosition = base.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (GameManager.Instance.State == EnumState.Playing)
		{
			base.transform.Translate(Vector3.right * (this.speed * Time.deltaTime));

			if (base.transform.position.x >= this.resetPosition.x)
			{
				//try with -42.85f
				base.transform.position = new Vector3(this.initialPosition.x, base.transform.position.y, base.transform.position.y);

				this.EnableCoins();
			}
		}
	}

	public void Restart()
	{
		this.ResetPosition();
		this.ResetRocks();
		this.EnableCoins();
	}

	private void ResetRocks()
	{
		foreach (Rock rock in base.GetComponentsInChildren<Rock>())
		{
			rock.ResetPosition();
		}
	}

	public void ResetPosition()
	{
		base.transform.position = this.startPosition;
	}

	private void EnableCoins()
	{
		if (this.listCoin != null)
		{
			foreach (Coin coin in this.listCoin)
			{
				coin.Enable();
			}
		}
	}
}
