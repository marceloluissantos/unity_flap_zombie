using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    [SerializeField]
    private Text textScore;
    private int score;

    void Awake()
    {
        Assert.IsNotNull(textScore);

        if (GameManager.instance == null)
        {
            GameManager.instance = this;
        }
        else
        {
            if (GameManager.instance != this)
            {
                MonoBehaviour.Destroy(base.gameObject);
            }
        }

        MonoBehaviour.DontDestroyOnLoad(base.gameObject);
    }

    public static GameManager Instance 
    { 
        get
        {
            return GameManager.instance;
        }
    }

    public void SetPlaying()
    {
        this.State = EnumState.Playing;
    }

    public EnumState State { get; set; }

    public int Score 
    { 
        get
        {
            return this.score;
        }
        set
        {
            this.score = value;
            this.textScore.text = string.Format("Score: {0}", this.score);
        }
    }
}